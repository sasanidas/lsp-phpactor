;;; lsp-phpactor.el --- Phpactor server configuration         -*- lexical-binding: t; -*-

;; Copyright (C) 2020 Fermin Munoz

;; Author: Fermin Munoz <fmfs@posteo.net>
;; Keywords: php lsp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; lsp-phpactor client

;;; Code:

(require 'lsp-protocol)
(require 'lsp-mode)

(defgroup lsp-phpactor nil
  "LSP support for the PHP programming language, using Phpactor"
  :group 'lsp-mode
  :link '(url-link "https://gitlab.com/sasanidas/lsp-phpactor")
  :package-version '(lsp-mode . "7.0"))

(defcustom lsp-phpactor-server-path
  "phpactor"
  "Path to the Phpactor Language Server."
  :group 'lsp-phpactor)


(defun lsp-phpactor-fun ()
  "Startup command for the Phpactor language server."
  (list  "/home/fermin/Programming/lsp-phpactor/vendor/bin/phpactor" "language-server"))

(defun lsp-phpactor-init-options ()
  "Init options for lsp-phpactor."
  `(:language_server.session_parameters []))

(lsp-register-client
 (make-lsp-client
  :new-connection (lsp-stdio-connection #'lsp-phpactor-fun)
  :major-modes '(php-mode)
  :initialization-options #'lsp-phpactor-init-options
  :priority -1
  :server-id 'phpactor))
